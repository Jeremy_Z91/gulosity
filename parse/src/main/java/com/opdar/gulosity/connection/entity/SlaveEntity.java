package com.opdar.gulosity.connection.entity;

/**
 * Created by Shey on 2016/8/21.
 */
public class SlaveEntity {
    private String file;
    private int position;
    private int slaveId;

    public int getSlaveId() {
        return slaveId;
    }

    public void setSlaveId(int slaveId) {
        this.slaveId = slaveId;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean checkBinlogFile(){
        return file != null && !file.equals("");
    }
}
